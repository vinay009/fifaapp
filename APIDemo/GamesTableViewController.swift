//
//  GamesTableViewController.swift
//  APIDemo
//
//  Created by MacStudent on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity
import SwiftyJSON

class GamesTableViewController: UITableViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    
    }
    

    var gameList:[Game] = []
    var gameSubList:[Game] = []
    var jsonResponse:JSON!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("------------------------------------")
        //print("\(jsonResponse.dictionary)")
        for (index, e) in self.gameList.enumerated() {
            print("\(e.Coordinates)")
            print("\(e.GameDate)")
            print("\(e.GameTime)")
            print("\(e.GameSection)")
            print("\(e.Location)")
            print("\(e.TeamA)")
            print("\(e.TeamB)")
        }
        print("------------------------------------")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gameList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! GamesTableViewCell

        // Configure the cell...
        
        
        
        
        cell.teamAImg.image = UIImage(named: gameList[indexPath.row].TeamA)
        cell.teamBImg.image = UIImage(named: gameList[indexPath.row].TeamB)
        cell.descLbl.text = "\(gameList[indexPath.row].GameSection!)"
        cell.desc2Lbl.text = "Long 7.26,lat 43.71"
        cell.desc3Lbl.text = "\(gameList[indexPath.row].GameDate!)"
        cell.desc4Lbl.text = "\(gameList[indexPath.row].GameTime!)"
        cell.desc5Lbl.text = "UnSubscribe"
        cell.desc5Lbl.textColor = UIColor.blue
        //cell.contentView.backgroundColor = UIColor.gray
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = indexPath.row

        print("Person clicked in row number: \(i)")
        print("\(gameList[indexPath.row].TeamA!) Vs \(gameList[indexPath.row].TeamB!)")
        
        
        if (gameList[indexPath.row].Subscribed!) {
            
            let cell = tableView.cellForRow(at: indexPath) as! GamesTableViewCell
            cell.desc5Lbl.text = "UnSubscribe"
            cell.desc5Lbl.textColor = UIColor.blue
            
        }else{
           
            let cell = tableView.cellForRow(at: indexPath) as! GamesTableViewCell
            cell.desc5Lbl.text = "Subscribed"
            cell.desc5Lbl.textColor = UIColor.green
        }
        gameList[indexPath.row].Subscribed = !gameList[indexPath.row].Subscribed!
        
        print(jsonResponse)
        
        //var json:String = "{\"id\": 24, \"name\": \"Bob Jefferson\", \"friends\": [{\"id\": 29, \"name\": \"Jen Jackson\"}]}"
        var json:String = "["
        //var listToSend:String = "["
        
        for (index, e) in self.gameList.enumerated() {
            
            json += "{\"gameSection\": \"\(e.GameSection!)\",\"gameDate\": \"\(e.GameDate!)\",\"gameTime\": \"\(e.GameTime!)\",\"location\": \"\(e.Location!)\",\"lat\": \"\(e.Coordinates.latitude)\",\"lon\": \"\(e.Coordinates.longitude)\",\"teamA\": \"\(e.TeamA!)\",\"teamB\": \"\(e.TeamB!)\",\"subscribed\": \"\(e.Subscribed!)\"}"
            
            if(index < self.gameList.count-1){
                json += ","
            }
            
        }
        json += "]"
        
        
        print("************")
        
        sendDataToWatch(listToSend: json)
    }
    
    func sendDataToWatch(listToSend : String) {
        
        print("daasda")
        let abc = ["gameList": listToSend]
        //let abc = listToSend
        if (WCSession.default.isReachable) {
            
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("PHONE: Cannot find the watch")
        }
        
    }

    
   
}
