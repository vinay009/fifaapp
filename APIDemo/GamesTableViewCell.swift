//
//  GamesTableViewCell.swift
//  APIDemo
//
//  Created by MacStudent on 2019-07-02.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit

class GamesTableViewCell: UITableViewCell {


    @IBOutlet weak var teamBImg: UIImageView!
    @IBOutlet weak var teamAImg: UIImageView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var desc2Lbl: UILabel!
    @IBOutlet weak var desc3Lbl: UILabel!
    @IBOutlet weak var desc4Lbl: UILabel!
    @IBOutlet weak var desc5Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
