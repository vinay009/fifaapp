//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    var gameList:[Game] = []
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print(message)
        print("------")
        print(message["gameList"])
        
        
        
        
        var gamesList2 = message["gameList"] as! String
  
        print("%%%%%%%%%")
        
        

        if let data = gamesList2.data(using: .utf8) {
            if let json = try? JSON(data: data) {
                print(json)
                for item in json.arrayValue {
                    print(item.dictionary)
                    
                    var game = item.dictionary
                    self.gameList.append(Game(gameSection: game!["gameSection"]!.string!, gameDate: game!["Date"]!.string!, gameTime: game!["Time"]!.string!, location: game!["location"]!.string!, coordinates: CLLocationCoordinate2D(latitude: 43.653, longitude: -79.283), teamA: game!["TeamA"]!.string!, teamB: game!["TeamB"]!.string!))
                    
                }
            }
        }
        

        
        
        
        print("DONE!")
        
    }
    
    // MARK: Outlet
    // ---------------
 
    
    // MARK: Actions
    @IBAction func getDataPressed() {
        print("Watch button pressed")
       
    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()) {
            print("WATCH: WCSession is supported!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WATCH: Does not support WCSession, sorry!")
        }
        
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
